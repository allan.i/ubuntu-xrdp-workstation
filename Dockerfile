FROM danielguerra/ubuntu-xrdp:20.04

RUN apt-get update \
    && apt-get install -y \
        docker.io \
        docker-compose \
    && wget https://release.gitkraken.com/linux/gitkraken-amd64.deb \
    && dpkg -i gitkraken-amd64.deb; exit 0 \
    && apt-get install -f -y \
    && rm gitkraken-amd64.deb \
    && rm -rf /var/lib/apt/lists/*

RUN wget -P /opt https://download.jetbrains.com/webide/PhpStorm-2020.3.3.tar.gz \
    && cd /opt \
    && tar xf PhpStorm-2020.3.3.tar.gz \
    && rm PhpStorm-2020.3.3.tar.gz



